﻿using System;
using System.Collections.Generic;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            string strInput = "abc#d##c";
            Stack<char> stack = new Stack<char>();

            foreach (var charInStack in strInput)
            {
                if (charInStack == '#')
                {
                    if (stack.Count > 0)
                        stack.Pop();
                }
                else
                {
                    stack.Push(charInStack);
                }
            }
            var array = stack.ToArray();
            Array.Reverse(array);
            string s = new string(array);
            Console.WriteLine(s);
        }
    }
}
