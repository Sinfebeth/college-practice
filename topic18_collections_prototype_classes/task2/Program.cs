﻿using System;
using System.Collections.Generic;
using static System.Console;
using System.Linq;
using System.Text;
using System.IO;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<char> symbols = new Queue<char>();
            Queue<char> digits = new Queue<char>();
            string sr = File.ReadAllText("1.txt");
            foreach (char s in sr)
            {
                if (char.IsDigit(s)) digits.Enqueue(s);
                else symbols.Enqueue(s);
            }
            Print(symbols, Console.WriteLine);
            Print(digits, Console.WriteLine);
        }
        private delegate string Output(Action<string> action);
        private static void Print(Queue<char> symbolsOrDigits, Action<string>action)
        {
            Console.WriteLine(string.Join(' ',symbolsOrDigits));
        }
    }
}