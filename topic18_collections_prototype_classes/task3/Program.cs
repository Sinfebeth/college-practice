﻿using System;
using static task3.Book;

namespace task3
{
    class Program
    {
        static void Main(string[] args)
        {
            INote<string> page = new Note<string>("Ogogogogogog info");
            INote<string> clonedPage = page.Clone();
            Console.WriteLine("Original note");
            Console.WriteLine(page.GetInfo());
            Console.WriteLine("Clone note");
            Console.WriteLine(clonedPage.GetInfo());

            if (clonedPage.LengthInfo.HasValue)
            {
                Console.WriteLine(clonedPage.LengthInfo);
            }
            Console.WriteLine("обнулить клонированную страницу");
            clonedPage.GetInfo();
        }
    }
}
