﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace task3
{
    class Book
    {
       public interface INote<T>
        {
            INote<T> Clone();
            string GetInfo();
            public int? LengthInfo { get; }
        }
        public class Note<T>: INote<T>
        {
            private T _information;
            private bool? _isImportent;
            private int? _lengthInfo;

            public int? LengthInfo => _lengthInfo;

            public T Information => _information;

            public Note(T information, bool? isImportent=null)
            {
                _information = information;
                _isImportent = isImportent;
            }  
            public INote<T> Clone()
            {
                return new Note<T>(_information);
            }
            public string GetInfo()
            {
                return $"Info is {_information}\nIs importent: {_isImportent??false}";
            }           
        }
    }
}
