﻿using System;
using System.Diagnostics;
using System.Threading;
using static task2.SumOfInts;

namespace task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sWatch = new Stopwatch();
            sWatch.Start();
                Thread thread1 = new Thread(() => Sum(Console.WriteLine));
                Thread thread2 = new Thread(() => Sum(Console.WriteLine));
                thread1.Start();
                thread2.Start();
            sWatch.Stop();
            Console.WriteLine($"time in ms = {sWatch.ElapsedMilliseconds.ToString()}");

        }
    }
}
