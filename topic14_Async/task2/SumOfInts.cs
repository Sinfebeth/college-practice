﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    public static class SumOfInts
    {
        public static void Sum(Action<string> action)
        {
            for (int i = 0; i < 10; i++)
            {
                action($"sum = {i += i}");
            }
        }
    }
}
