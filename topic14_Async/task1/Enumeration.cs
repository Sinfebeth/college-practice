﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    public static class Enumeration
    {
        public static void IntEnum (int begin, int end, Action<string> action)
        { 
            for (int i = begin; i < end; i++)
            {
                action ($"поток перечесляет {i}");
            }
        }
    }
}
