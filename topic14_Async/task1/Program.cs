﻿using System;
using System.Threading;
using static task1.Enumeration;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread thread1 = new Thread(()=> IntEnum(0, 10, Console.WriteLine));
            Thread thread2 = new Thread(()=> IntEnum(10, 20, Console.WriteLine));
            Thread thread3 = new Thread(()=> IntEnum(20, 30, Console.WriteLine));
            thread1.Start();
            thread1.Join();
            thread2.Start();
            thread2.Join();
            thread3.Start();
            thread3.Join();
        }
    }
}
