﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace task3
{
    public class Methods
    {
        public delegate void ShowInfo(Action<string> action);
        public static void FirstMethod(int a, int n, Action<string> action)
        {
            double answer = 0;
            for (int i = 1; i < n; i++)
            {
                answer += Math.Pow(a, i);
            }

            action ($"Сумма = {answer}\n");

        }

        public static void SecondMethod(int a, int n, Action<string> action)
        {
            double answer = 1;
            for (int i = 1; i < n; i++)
            {
                answer *= Math.Pow(a, i);
            }

            action($"Произведение = {answer}\n");
        }
    }
}
