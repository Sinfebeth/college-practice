﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    class Presenter
    {
        public static void Print(string message) => Console.WriteLine(message);
    }
}
