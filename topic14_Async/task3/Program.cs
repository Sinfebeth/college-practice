﻿using System;
using static System.Console;
using static System.Convert;
using System.Threading;
using System.Diagnostics;
using static task3.Methods;
using static task3.Presenter;

namespace Задание_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Write("Введите А:");
            var a = ToInt32(ReadLine());
            Write("Введите N:");
            var n = ToInt32(ReadLine());

            Thread firstT = new Thread(new ThreadStart(() => FirstMethod(a, n,Print)));
            Thread secondT = new Thread(new ThreadStart(() => FirstMethod(a, n,Print)));

            Thread thirdT = new Thread(new ThreadStart(() => SecondMethod(a, n,Print)));

            firstT.Start();
            secondT.Start();
            thirdT.Start();

            ReadKey();

        }
    }
}
