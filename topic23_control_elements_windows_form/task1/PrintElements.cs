﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    class PrintElements
    {
        public static void Print(int[] mas, Action<string> action)
        {
            action.Invoke("elements mod 2=0: ");
            for (int i = 0; i < mas.Length; i++)
            {
                action.Invoke($"{mas[i]} ");
            }
        }
    }
}
