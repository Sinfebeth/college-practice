﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace task1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int index = listBox1.SelectedIndex;
            int[] numbers = listBox1.Items[index]
                .ToString().Split(' ')
                .Select(x => int.Parse(x))
                .Where(x=>x%2==0)
                .ToArray();
            PrintElements.Print(numbers, SetLabelText);
        }
        private void SetLabelText(string input)
        {
            label1.Text += input;
        }
    }
}
