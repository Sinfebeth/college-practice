﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace task4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void button1_Click(object sender, EventArgs e)
        {
            int a = Convert.ToInt32(textBox2.Text);
            int b = Convert.ToInt32(textBox3.Text);

            switch (textBox1.Text)
            {
                case "к":
                    {
                        Button butt = new Button();
                        butt.Location = new Point(a, b);
                        this.Controls.Add(butt);
                        butt.MouseMove += ctrButton_MouseMove;
                    }
                    break;
                case "м":
                    {
                        Label lab = new Label();
                        lab.Location = new Point(a, b);
                        lab.Text = "Метка";
                        this.Controls.Add(lab);
                        lab.MouseMove += ctrLabel_MouseMove;
                    }
                    break;
                case "л":
                    {
                        TextBox textb = new TextBox();
                        textb.Location = new Point(a, b);
                        this.Controls.Add(textb);
                        textb.MouseMove += ctrTextbox_MouseMove;
                    }
                    break;
            }
        }
        private void ctrButton_MouseMove(Object sender, MouseEventArgs e)
        {
            Button b = (Button)sender;
            b.Dispose();
        }
        private void ctrTextbox_MouseMove(Object sender, MouseEventArgs e)
        {
            TextBox t = (TextBox)sender;
            t.Dispose();
        }
        public void ctrLabel_MouseMove(Object sender, MouseEventArgs e)
        {
            Label l = (Label)sender;
            l.Dispose();
        }
    }
}
