﻿using static task1.Delegate;
using static task1.Presenter;
namespace task1
{
    class MathMethods
    {
        public delegate void NotifyMessage(string message);
        public static event NotifyMessage Notify;
        public static int Sum()
        {
            int a = 15;
            int b = 30;
            Notify?.Invoke($"совершёл плюс) {a+b}");
            return a + b;
        }
        public static int Sub()
        {
            int a = 15;
            int b = 30;
            Notify?.Invoke($"совершёл минус) {a-b}");
            return a - b;
        }
        public static int Mult()
        {
            int a = 15;
            int b = 30;
            if (Notify!=null)
            {
                Notify($"совершено умножение) {a * b}");
            }            
            return a * b;
        }
    }
}
