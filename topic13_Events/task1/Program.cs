﻿using System;
using static task1.Delegate;
using static task1.MathMethods;
using static System.Console;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            MathMethods.Notify += Presenter.DisplayMessage;
            WriteLine($"In Main message substraction {Sub()}");
            WriteLine($"In Main message substraction {Sub()}");
            WriteLine($"In Main message substraction {Sub()}");
        }
    }
}
