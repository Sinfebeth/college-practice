﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double x = Convert.ToDouble(textBox2.Text);
            double i = Convert.ToDouble(textBox1.Text);
            double func=0;
            textBox4.Text += "При i = " + textBox2.Text + Environment.NewLine;
            textBox4.Text += "При x = " + textBox1.Text + Environment.NewLine;
            double u;
            switch (checkedListBox1.SelectedIndex)
            {
                case -1:
                    func = 0;
                    break;
                case 0:
                    func = Math.Pow(x, 2);
                    break;
                case 1:
                    func = Math.Pow(Math.E, x);
                    break;
                case 2:
                    func = Math.Sinh(x);
                    break;
            }
            if (i%2==1 && x>0)
                u = i *Math.Sqrt(func);
            else
            if (i%2==0 && x< 0)
                u = i/2 * Math.Sqrt(func);
            else
                u = Math.Sqrt(i*func);
            textBox4.Text += "U = " + u.ToString() + Environment.NewLine;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox4.Text = "";
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
