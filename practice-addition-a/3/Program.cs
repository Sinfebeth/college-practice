﻿using System;

namespace _3
{
    class Program
    {
        static void Main(string[] args)
        {
            const double x = 0.5;
            const double e = 2.71;
            double y;
            y = (x * Math.Pow(e, Math.Pow(x, 2))) - ((Math.Sin(Math.Sqrt(x))) + Math.Pow(Math.Cos(Math.Log10(x)), 2) / Math.Sqrt(Math.Abs(1 - Math.PI * x)));
            Console.WriteLine(y);
        }
    }
}
