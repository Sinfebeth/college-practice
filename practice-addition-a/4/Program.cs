﻿using System;

namespace _4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите сопротивление: ");
            Console.WriteLine("Введите сопротиваление r1");
            double r1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите сопротиваление r2");
            double r2 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите сопротиваление r3");
            double r3 = Convert.ToDouble(Console.ReadLine());
            double R = (r1*r2*r3)/(r1+r2+r3);
            Console.WriteLine("Общее сопротивление равно {0}", R);
        }
    }
}
