﻿using System;
using System.Windows.Forms;

namespace task1
{
    public partial class Form1 : Form
    {
        private int openDocuments = 0;
        public Form1()
        {
            InitializeComponent();
            toolStripStatusLabel2.Text=Convert.ToString(System.DateTime.Today.ToLongDateString());
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Tag.ToString())
            {
                case "NewDoc":
                    ChildForm newChild = new ChildForm();
                    newChild.MdiParent = this;
                    newChild.Show();
                    newChild.Text = newChild.Text + " " + ++openDocuments;
                    break;
                case "Cascade":
                    this.LayoutMdi(System.Windows.Forms.MdiLayout.Cascade);
                    break;
                case "Title":
                    this.LayoutMdi
                    (System.Windows.Forms.MdiLayout.TileHorizontal);
                    break;
            }
        }

        private void spWin_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            spWin.Text = "Windows is cascade";
            toolStripStatusLabel1.Text = "status1";
        }
    }
}
