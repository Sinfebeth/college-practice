﻿using System;
namespace task1
{
    class Auto
    {
        protected string _name;
        protected int _fuel;
        public int Fuel => _fuel;
        public string Name => _name;
        public Auto(string name, int fuel)
        {
            _name = name;
            _fuel = fuel;
        }
        public virtual double InternalQualities(int qualityOfCar)
        {
            throw new NotImplementedException();
        }
        public double FuelConsumption(int fuel, int distance)
        {
            return (double)fuel / distance;
        }
    }
   
}
