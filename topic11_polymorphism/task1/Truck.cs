﻿using System;
namespace task1
{
    class Truck : Auto
    {
        public int _loadCapacity { get; set; }
        public Truck(string name, int loadCapacity,int fuel) : base(name,fuel)
        {
            _name = name;
            _loadCapacity = loadCapacity;
        }
        public override double InternalQualities(int loadCapacity)
        {
            return Math.Sqrt(loadCapacity)*100;
        }
    }
}
