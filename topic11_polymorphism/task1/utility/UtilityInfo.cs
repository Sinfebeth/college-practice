﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1.utility
{
    static class UtilityInfo
    {
        public static void Info(Truck auto)
        {
            Console.WriteLine($"имя машины: {auto.Name}, вместимость: {auto.InternalQualities(auto._loadCapacity):F2} расход топлива на 100км: {auto.FuelConsumption(auto.Fuel, 100)}");
        }
        public static void Info(PassagerCar auto)
        {
            Console.WriteLine($"имя машины: {auto.Name}, вместимость: {auto.InternalQualities(auto._engineCapacity)} расход топлива на 100км: {auto.FuelConsumption(auto.Fuel,100)}");
        }
    }
}
