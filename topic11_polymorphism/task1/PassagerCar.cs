﻿using System;
namespace task1
{
    class PassagerCar:Auto
    {
        public int _engineCapacity { get; set; }
        public PassagerCar(string name, int engineCapacity, int fuel):base(name,fuel)
        {
            _name = name;
            _engineCapacity = engineCapacity;
            _fuel = fuel;
        }
        public override double InternalQualities(int engineCapacity)
        {
            return 2.5 * engineCapacity;
        }
    }
}
