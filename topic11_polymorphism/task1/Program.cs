﻿using System;
using task1.utility;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            PassagerCar passagerCar = new PassagerCar("reno", 23,24);
            Truck truck = new Truck("Tesla Truck", 15,55);
            UtilityInfo.Info(truck);
            UtilityInfo.Info(passagerCar);
        }
    }
}
