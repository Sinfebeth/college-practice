﻿using System;

namespace ConsoleApp1
{
    class A
    {
        int a;
        int b;
        public A (int a, int b)
        {
            this.a = a;
            this.b = b;
        }
        public double average (int a,int b)
        {
            return (a + b) / 2;
        }
        public double expression(int a, int b)
        {
            return Math.Pow(b, 3) + Math.Sqrt(a);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("введдите a и b");
            string[] inputs = Console.ReadLine().Split(' ',StringSplitOptions.RemoveEmptyEntries);
            int a = int.Parse(inputs[0]);
            int b = int.Parse(inputs[1]);//зачем так, захотелось) можно было проще
            A class_a = new A(a,b);
            Console.WriteLine(class_a.average(a,b));
            Console.WriteLine(class_a.expression(a,b));
        }
    }
}
