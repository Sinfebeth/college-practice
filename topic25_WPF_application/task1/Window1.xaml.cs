﻿using System.Windows;

namespace task1
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
            Button1.Click += Button1_Click;
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        public Window1 (string appInfo) : this()
        {
            TB.Text = appInfo;
        }
    }
}
