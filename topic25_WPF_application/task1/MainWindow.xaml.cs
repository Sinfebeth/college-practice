﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace task1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Help.Click += Help_Click;
        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            string Info = $"about app{this.Title}";
            Window1 aboutApp = new Window1(Info);
            aboutApp.ShowDialog();
        }

        private void Button_Run_Click(object sender, RoutedEventArgs e)
        {
            string hello = "Hello";
            string input = string.Empty;
            if (string.IsNullOrEmpty(TextBox_EnterName.Text)|| string.IsNullOrEmpty(TextBlock_Hello.Text))
            {
                input = "World";
            }
            else
            {
                input = TextBox_EnterName.Text;
            }
            TextBlock_Hello.Text = $"{hello} {input}";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
