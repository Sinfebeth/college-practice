﻿namespace task1
{
    interface IWaterState
    {
        void Heat(Water water);
        void Frost(Water water);
    }
}
