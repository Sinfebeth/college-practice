﻿using System;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            Water water = new Water(new SolidWaterState());
            water.Heat();
            water.Frost();
            water.Frost();
        }
    }
}
