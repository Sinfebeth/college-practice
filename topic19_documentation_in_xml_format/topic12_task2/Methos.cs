﻿using System;

namespace task4
{
    class Methos
    {/// <summary>
    /// returns random number (-10, 10)
    /// </summary>
    /// <param name="action"></param>
    /// <returns>int random number</returns>
        public static int Method1(Action<string> action)
        {
            Random rnd = new Random();
            int nextRand = rnd.Next(-10, 10);
            action($"число = {nextRand}");
            return nextRand;
        }
        /// <summary>
        /// returns random number (-10, 10)
        /// </summary>
        /// <param name="action"></param>
        /// <returns>int random number</returns>
        public static int Method2(Action<string> action)
        {
            Random rnd = new Random();
            int nextRand = rnd.Next(-10, 10);
            action($" следующее число = {nextRand}");
            return nextRand;
        }
    }
}
