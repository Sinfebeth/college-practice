﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace task1
{
    class Program
    {
        private const string Input = "test_1", Output = "test";
        private const int MinValue = 0, MaxValue = 20;
        private const int Count = 10;

        private static Random _rand = new();

        static void Main(string[] args)
        {
            var generatedValues = GenerateRandomSequence(Count, MinValue, MaxValue);

            SaveToFile(generatedValues, Input, "test_1.txt");

            var values = ReadFromFile(Input);
            var result = GetNumbersMultiplesOfThree(values);

            SaveToFile(result, Output, "test.txt");
        }

        private static IEnumerable<int> GenerateRandomSequence(int count, int MinValue, int MaxValue)
        {
            return Enumerable
                .Range(0, count)
                .Select(value => _rand.Next(MinValue, MaxValue));
        }

        private static IEnumerable<int> GetNumbersMultiplesOfThree(IEnumerable<int> values)
        {
            return values
                .Where(value => value % 3 == 0);
        }

        private static void SaveToFile(IEnumerable<int> values, string foler, string name)
        {
            var path = Path.Combine(foler, name);
            var value = string.Join(",", values);
            File.WriteAllText(path, value);
        }

        private static IEnumerable<int> ReadFromFile(string path)
        {
            return File
                .ReadAllText(path)
                .Split(",")
                .Select(value => int.Parse(value));
        }
    }
}
