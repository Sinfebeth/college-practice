﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static System.Console;

namespace task3
{
    class Program
    {
        private const string Input = "input.txt";
        static void Main(string[] args)
        {
            var values = GetAllStrings(Input);
            var count = values.Count();
            var dictLines = FindOutCountCharsForEachLine(values);
            PrintCountOfCharsInEachLine(dictLines);
            DeleteLastString(values);
            GetStringsFromS1ToS2(Input, 2, 4);
            GetLongestLine(Input);
            PrintToFileReverseLinesInfile(Input);
            GetLineWithFirstLetter(Input, 'e');
        }

        private static Dictionary<int, int> FindOutCountCharsForEachLine(IEnumerable<string> values)
        {
            return values
                .Select((value, index) => new
                {
                    Index = index,
                    Length = value.Length,
                })
                .ToDictionary(
                    keySelector => keySelector.Index,
                    valueSelector => valueSelector.Length);
        }
        private static void PrintCountOfCharsInEachLine(Dictionary<int, int> charDict)
        {
            foreach(var index in charDict.Keys)
            {
                WriteLine($"строка - {index}, кол-во букв - {charDict[index]}");
            }
        }
        private static void DeleteLastString(IEnumerable<string> values)
        {
            string path = "output_part_d.txt";
            File.WriteAllLines(path, values.SkipLast(1));
        }
        private static string GetStringsFromS1ToS2(string path, int fromS1, int toS2)
        {
            return Convert.ToString(File.ReadAllLines(path)
                .Skip(fromS1)
                .Take(File.ReadAllLines(path).Length-toS2));
        }
        private static string GetLongestLine(string path)
        {
            return Convert.ToString(File.ReadAllLines(path)
                .Where(s => s.Length == File.ReadAllLines(path).Max(m => m.Length))
                .First());
        }
        private static string GetLineWithFirstLetter(string path, char firstChar)
        {
            return Convert.ToString(File.ReadAllLines(path)
                .Where(s => s[0] == firstChar));
        }
        private static void PrintToFileReverseLinesInfile(string path)
        {
            string pathoutput = "output_reverse.txt";
            File.WriteAllLines(pathoutput, File.ReadAllLines(path).Reverse());
        }
        private static IEnumerable<string> GetAllStrings(string path)
        {
            return File.ReadAllLines(path);
        }
    }
}
