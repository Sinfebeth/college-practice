﻿using System;

namespace task2
{
    class Accauntant
    {
        private static string[] _position;
        private static int[] _hoursWorked;

        public static string[] Personnel()
        {
            string[] personnel = new string[6];
            for (int i = 0; i < 6; i++)
            {
                personnel[i] = Faker.Name.First();
            }
            return personnel;
        }
        public static void SetPesonelInfo(string[] personel)
        {
            _position = new string[6];
            _hoursWorked = new int[6];
               
             for (int i = 0; i < personel.Length; i++)
             {
                
                Console.WriteLine($"имя {personel[i]} присвойте должность \n 1 - manager \n 2 - продавец \n 3 - консультант");
                int posNumber = int.Parse(Console.ReadLine());
                switch (posNumber)
                {
                    case 1:
                        _position[i] = Enum.GetName(HoursOfWork.manager);
                        Console.WriteLine("введите отработанное время");
                        _hoursWorked[i] = int.Parse(Console.ReadLine());
                        break;
                    case 2:
                        _position[i] = Enum.GetName(HoursOfWork.salesman);
                        Console.WriteLine("введите отработанное время");
                        _hoursWorked[i] = int.Parse(Console.ReadLine());
                        break;
                    case 3:
                        _position[i] = Enum.GetName(HoursOfWork.consultant);
                        Console.WriteLine("введите отработанное время");
                        _hoursWorked[i] = int.Parse(Console.ReadLine());
                        break;
                }
             }
        }
        public static void PrintPersonelInfo(string[] personel)
        {
            string[] position = _position;
            for (int i = 0; i < personel.Length; i++)
            {
                Console.WriteLine($"Имя {personel[i]}, должность {_position[i]}, отработано часов {_hoursWorked[i]}, доступ к бонусу {AskForBonus(_position, _hoursWorked, i)}");
            }
        }
        private static bool AskForBonus(string[] position, int[] hoursWorked, int i)
        { 
            var hours = Enum.Parse(typeof(HoursOfWork), position[i]);
            return (Convert.ToInt32(hours) >= hoursWorked[i]);
        }
    }
}
