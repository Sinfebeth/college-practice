﻿namespace lab7
{
    partial class Program
    {
        struct Aeroflot
        {
            public string pointNaznach;
            public int numberReis;
            public AirType airType;

            public Aeroflot(string pointNaznach, int numberReis, AirType airType)
            {
                this.pointNaznach = pointNaznach;
                this.numberReis = numberReis;
                this.airType = airType;
            }
            public override string ToString()
            {
                return $"{pointNaznach} | {numberReis} | {airType}";
            }
        }


    }
}
