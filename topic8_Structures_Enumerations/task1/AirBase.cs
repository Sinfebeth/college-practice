﻿using System;

namespace lab7
{
    partial class Program
    { 
        class AirBase
        {
            private Aeroflot[] _aeroFlots;
            public void AddAeroFlot(Aeroflot aeroFlot)
            {
                if (_aeroFlots == null)
                {
                    _aeroFlots = new Aeroflot[1];
                }
                else
                {
                    var count = _aeroFlots.Length;
                    Array.Resize<Aeroflot>(ref _aeroFlots, count + 1);
                }
                _aeroFlots[^1] = aeroFlot;
            }
            public Aeroflot[] GetAllAeroFlots()
            {
                return _aeroFlots;
            }
        }


    }
}
