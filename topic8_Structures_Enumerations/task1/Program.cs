﻿using System;

namespace lab7
{
    partial class Program
    {

        public static void Main()
        {
            AirBase airBase = new AirBase();
            for (int i = 0; i < 2; i++)
            {
                string[] inputs = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries);
                AirType airtype = (AirType)Enum.ToObject(typeof(AirType), int.Parse(inputs[0])); //int.Parse(inputs[0]);
                int numberReis = int.Parse(inputs[1]);
                string pointNaznach = inputs[2];
                Aeroflot aeroFlot = new Aeroflot() { airType = airtype, numberReis = numberReis, pointNaznach = pointNaznach };
                airBase.AddAeroFlot(aeroFlot);

            }
            var aeroFlots = airBase.GetAllAeroFlots();
            foreach (var aeroFlotItem in aeroFlots)
            {
                Console.WriteLine(aeroFlotItem);
            }
            
        }


    }
}
