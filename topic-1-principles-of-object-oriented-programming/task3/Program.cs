﻿using System;
using Faker;

namespace _3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("введите пол м/ж");
            char gender = char.Parse(Console.ReadLine());
            if (gender=='м')
            {
                for (int i = 0; i < 3; i++)
                {
                    Console.WriteLine( Faker.NameFaker.MaleFirstName());
                }
            }
            else
            {
                for (int i = 0; i < 3; i++)
                {
                    Console.WriteLine(Faker.NameFaker.FemaleFirstName());
                }
            }
        }
    }
}
