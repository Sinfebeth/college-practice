﻿using System;

namespace _4
{
    class Program
    {
        static void Main(string[] args)
        {
            int x;
            int z;
            int y = 10;
            Console.Write("Введите стоимость товара(целое число): ");
            x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Цикл while.");
            while(y<201)
            {
                z = y * x;
                Console.WriteLine("Цена за {0} единиц товара = {1} у.е.",y,z);
                y += 10;
            }
            Console.WriteLine("Цикл do while.");
            y = 10;
            z = 0;
            do
            {
                z = y * x;
                Console.WriteLine("Цена за {0} единиц товара = {1} у.е.", y, z);
                y += 10;
            }
            while (y < 201);
            Console.WriteLine("Цикл for.");
            z = 0;
            for (int m = 10; m < 201; m += 10)
            {
                z = m * x;
                Console.WriteLine("Цена за {0} единиц товара = {1} у.е.", m, z);
            }

        }
    }
}
