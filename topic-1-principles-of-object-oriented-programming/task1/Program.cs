﻿using System;

namespace _1_
{
    class Program
    {
        static void Main(string[] args)
        {
            double x;
            int y;
            Console.Write("Введтте число X: ");
            x = Convert.ToDouble(Console.ReadLine());
            if (x>1)
            {
                y = (int)(Math.Log10(x) * 2 + Math.Sqrt(1 + Math.Pow(x, 2)));
                Console.WriteLine("у = {0}", y);
            }
            else if (x<= 1)
            {
                y = (int)(2* Math.Cos(x)+3*Math.Pow(x,2));
                Console.WriteLine("y = {0}", y);
            }
        }
    }
}
