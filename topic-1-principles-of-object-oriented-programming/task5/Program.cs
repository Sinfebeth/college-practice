﻿using System;

namespace _5
{
    class Program
    {
        static void Main(string[] args)
        {
            double a = double.Parse(Console.ReadLine());
            int n = int.Parse(Console.ReadLine());

            for (int i = n; i < a; i++)
            {
                double sum = 1 + a + Math.Pow(a, i);
                Console.WriteLine(sum);
            }
        }
    }
}
