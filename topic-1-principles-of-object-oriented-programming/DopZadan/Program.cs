﻿using System;

namespace DopZadan
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите число: ");
            int number = Convert.ToInt32(Console.ReadLine());
            int n = 1;
            while (number > 1)
            {
                if (number % 2 == 0) number = number / 2;
                else number = 3 * number + 1;
                n++;
            }
            Console.WriteLine($"Число шагов равно {n}");

        }
    }
}
