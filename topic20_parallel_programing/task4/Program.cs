﻿using System;
using System.Threading;
using System.Threading.Tasks;
namespace task4
{
    class Program
    {
        static void Main(string[] args)
        {
            Parallel.For(-6, 4, CalculationFunc);

        }
        static void CalculationFunc(int x)
        {
            double result = 1;
            for (int i = 1; i < x; i++)
            {
                result = 1 / Math.Cos(x);
            }
            Console.WriteLine($"Ctg = {result} x = {x}");
            Thread.Sleep(3000);
        }
    }
}
