﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace task5
{
    class Program
    {
        static void Main(string[] args)
        {
            ParallelLoopResult result1 = Parallel.ForEach<int>(new List<int> () { 11, 12, 15, 68 }, Sum);
            ParallelLoopResult result2 = Parallel.ForEach<int>(new List<int>() { 11, 12, 15, 68 }, Mul);
        }
        static void Sum(int x)
        {
            int result = 0;
            for (int i = 0; i < x; i++)
            {
                result += i;
            }
            Console.WriteLine($"sum {result}");
        }
        static void Mul(int x)
        {
            int result = 1;
            for (int i = 0; i < x; i++)
            {
                result *= i;
            }
            Console.WriteLine($"Multiply {result}");
        }
    }
}
