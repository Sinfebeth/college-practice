﻿using System;
using System.Threading;
using System.Threading.Tasks;
using static task1.FindNumber;

class Program
{
    static void Main(string[] args)
    {
        Task task1 = new Task(() => Console.WriteLine($"Changed int 245 = {ChangeNumberPlace(245)}"));
        task1.Start();
        Task task2 = Task.Factory.StartNew(() =>Console.WriteLine($"Changed int 245 = {ChangeNumberPlace(245)}"));
        Task task3 = Task.Run(() => Console.WriteLine($"Changed int 245 = {ChangeNumberPlace(245)}"));
        task3.Wait();
    }
}