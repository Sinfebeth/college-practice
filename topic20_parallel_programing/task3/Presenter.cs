﻿using System;
using System.Threading;
using System.Threading.Tasks;
using static task3.ChangeNumber;

namespace task3
{
    public static class Presenter
    {
        public static void PrintChangedNumber(int number)
        {
            Console.WriteLine($"изменённое число = {ChangeNumberPlace(number)}");
            Thread.Sleep(1000);
        }
    }
}
