﻿using System;
using System.Threading.Tasks;
using static task3.ChangeNumber;
using static task3.Presenter;

namespace task3
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 24;
            Task task1 = new Task(() => ChangeNumberPlace(number));
            task1.Start();
            Task task2 = task1.ContinueWith(PrintChangedNumber(number));
            task1.Start();

        }
    }
}
