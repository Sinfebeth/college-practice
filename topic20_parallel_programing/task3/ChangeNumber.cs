﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    class ChangeNumber
    {
        public static int ChangeNumberPlace(int number)
        {
            return number % 10 * 10 + (number/ 10);
        }
    }
}
