﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Math;

namespace task2
{
    class CalculationFunction
    {
        public static double Calculation(int a)
        {
            double out1 = Sin(4 * a) / (1 + Sin(2 * a));
            double out2 = Cos(2 * a) / (1 + Cos(2 * a));
            return out1 * out2;
        }
        public static void PrintCalculationResult()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"итерация i={i} вывод = {Calculation(i)}");
            }
        }
    }
}
