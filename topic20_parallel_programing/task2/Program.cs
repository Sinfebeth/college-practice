﻿using System;
using System.Threading;
using System.Threading.Tasks;
using static task2.CalculationFunction;

namespace task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Task[] tasks = new Task[2]
                {
                    new Task (()=>PrintCalculationResult()),
                    new Task (()=>{
                        Thread.Sleep(10000);
                        PrintCalculationResult();}) }; 
            foreach (var t in tasks)
                t.Start();
            Task.WaitAll(tasks);
        }
    }
}
