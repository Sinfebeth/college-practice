﻿using System;
using static task3.MethodsString;
using static task3.Inputs;

namespace task3
{
    class Program
    {
        delegate void ParamsDelegate(Action<string> action, params string[] input);
        static void Main(string[] args)
        {
            var del = new ParamsDelegate(AddString);
            del += CharSubstract;
            del += Get_Length;
            del(Console.WriteLine, InputString(), InputString());
        }
    }
}
