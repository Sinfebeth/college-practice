﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    class MethodsString
    {
        public static void Get_Length( Action<string> action, params string[] s)
        {
            action($"Length= {s[0].Length}");
        }
        public static void AddString( Action<string> action, params string[] args)
        {
            var a = args[0];
            var b = args[1];
            action($"String Add= {a+b}");
        }
        public static void CharSubstract ( Action<string> action, params string[] args)
        {
            var a = args[0];
            var s = args[1];
            var indexIfA = s.IndexOf(a);
           if(indexIfA>=0) s.Remove(indexIfA, a.Length);
            action($"string without char {a}= {s}");
        }
    }
}
