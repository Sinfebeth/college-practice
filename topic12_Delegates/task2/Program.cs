﻿

using System;

namespace task2
{
    class Program
    {
        static void Main(string[] args)
        {
             var calc = new CalcFigure((x, y, action) => action($"{x + y}"));
            calc += (x, y, action) => action($"{x * y}");
            calc += (x, y, action) => action($"{x - y}");
            calc += (x, y, action) => { if (y != 0) { action($"{x / y}"); } else { throw new DivideByZeroException(); }};
            calc(5, 8, Console.WriteLine);
        }
    }
}
