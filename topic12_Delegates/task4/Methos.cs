﻿using System;

namespace task4
{
    class Methos
    {
        public static int Method1(Action<string> action)
        {
            Random rnd = new Random();
            int nextRand = rnd.Next(-10, 10);
            action($"число = {nextRand}");
            return nextRand;
        }
        public static int Method2(Action<string> action)
        {
            Random rnd = new Random();
            int nextRand = rnd.Next(-10, 10);
            action($" следующее число = {nextRand}");
            return nextRand;
        }
    }
}
