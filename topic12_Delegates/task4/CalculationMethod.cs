﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    class CalculationMethod
    {
        public static double Calc (params int[] randIntMas)
        {
            double average = 0;
            for (int i = 0; i < randIntMas.Length; i++)
            {
                average += randIntMas[i];
            }
            return average / randIntMas.Length;
        }
    }
}
