﻿using System;
using static task4.Delegates;
using static task4.Methos;
using static task4.Presenter;
using static task4.CalculationMethod;
namespace task4
{
    class Program
    {
        static void Main(string[] args)
        {
            MasDel[] masDel = new MasDel[]
            {
                new MasDel(Method1),
                new MasDel(Method2)
            };
            double result = Calc(
                masDel[0](Console.WriteLine),
                masDel[1](Console.WriteLine)
                );
            Output(result, Console.WriteLine);
        }
    }
}
