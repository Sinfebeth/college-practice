﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    class Presenter
    {
        public static void Output(double average, Action<string> action)
        {
            action($"Среднее арифметическое = {average}");
        }
    }
}
