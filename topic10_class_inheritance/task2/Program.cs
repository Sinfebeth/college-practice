﻿using System;

namespace task2
{
    class Program
    {
        static void Main(string[] args)
        {
            A classA = new A();
            B classB = new B(5);
            Console.WriteLine($"класс а, поле с = {classA.c}");
            Console.WriteLine($"класс б, поле с2 = {classB.c2}");
        }
    }
}
