﻿using System;

namespace task2
{
    public class Game
    {
        private string Name;
        private string Genre;
        public Game(string name, string genre)
        {
            this.Genre = genre;
            this.Name = name;
        }
        public void Info(Action<string> action)
        {
            action($"игра - {this.Name} в жанре - {this.Genre}");
        }
    }
}
