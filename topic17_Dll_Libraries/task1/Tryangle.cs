﻿using System;
using System.Linq;
using static System.Console;
using static System.Math;

namespace task1
{
    public class Tryangle
    {
        public static int[] Input()
        {
            int[] triangle = new int[3];
            for (int i = 0; i < 3; i++)
            {
                Write($"\nВведите {i} сторону триугольника = ");
                triangle[i] = Convert.ToInt16(ReadLine());
            }
            while (!isTriangle(triangle))
            {
                Write("такого треугольника существовать не может, введите снова");
                Input();
            }
            return triangle;
        }
        private static bool isTriangle(int[] triangle)
        {
            int a = triangle[0];
            int b = triangle[1];
            int c = triangle[2];

                if (a + b > c && a + c > b && b + c > a)
                {
                    return true;
                }
                return false;
        }
        public int Perimetr(int[] triangle)
        {
            return triangle.Sum();
        }
        public double Square(int[] triangle)
        {
            int a = triangle[0];
            int b = triangle[1];
            int c = triangle[2];
            int p = triangle.Sum() / 2;
            return Sqrt(p * (p - a) * (p - b) * (p - c));
        }
        public void Type(int[] triangle)
        {
            int a = triangle[0];
            int b = triangle[1];
            int c = triangle[2];
            if (Pow(c, 2) == Pow(a, 2) + Pow(b, 2)) Write("равнобедренный");
            else if (Pow(c, 2) < Pow(a, 2) + Pow(b, 2)) Write("остроугольный");
            else Write("тупоугольный"); 
        }
    }
}
