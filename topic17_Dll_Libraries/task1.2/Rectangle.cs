﻿using System;

namespace task1._2
{
    public class Rectangle
    {
        private int A;
        private int B;
        public Rectangle(int a, int b)
        {
            this.A = a;
            this.B = b;
        }
        public int perimetr(Rectangle rect)
        {
            return (rect.A + rect.B) * 2;
        }
        public double square (Rectangle rect)
        {
            return rect.A * rect.B;
        }
    }
}
